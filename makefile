cc =clang++

cf = -O3 -std=c++14

src = src/main.cpp src/tangler.cpp

bin = bin/tangle

release: $(src)
	$(cc) $(cf) $(src) -o $(bin)	
