#include "tangler.hpp"

//TODO
//header tangle
//tangle to multiple files
//count how many blocks were tangled
//evaluate or compile the block
//insert a line break between each block

void tangler::read()
{
	ifstream fil( file );
	string s;

	while( getline( fil, s ) )
	{
		fullText.push_back(s);
	}
}

void tangler::parser()
{
	for( int i = 0; i < fullText.size(); ++i)
	{
		if( fullText.at(i) >= "~~~ tangle: " )
		{
			save = fullText[i];
			i++;
			while( fullText.at(i) != "~~~" )
			{
				token.push_back( fullText.at(i) );
				++i;
			}
		}
	}

	ofile = save.substr(12,save.length());
}

void tangler::write()
{
	read();
	parser();

	ofstream fil( ofile );
	for( auto i:token )
		fil << i << endl;

	fil.close();
}

