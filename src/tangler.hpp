#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

class tangler
{
protected:
	string file, ofile, save;
	vector<string> fullText, token;
	
public:
	tangler( string f ) : file( f ) {};
	void read(); 
	void parser();
	void write();
};
